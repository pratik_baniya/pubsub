import java.io.IOException;
import java.util.List;

import org.apache.log4j.PropertyConfigurator;

import com.google.api.services.pubsub.Pubsub;

import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
public class SubThread extends Thread {

	private boolean cancelled;
	private String[] arguments;
	private Pubsub pubsub;
	private List<String> messages;
	private static final Logger logger = LoggerFactory.getLogger(MainSubscriber.class);

	public SubThread(Pubsub pbsub, String[] args) {

		pubsub = pbsub;
		arguments = args;
		PropertyConfigurator.configure("src/main/resources/log4j.properties");
	}//Constructor

	@Override
	public void run() {
		if (arguments.length == 2){
			
		logger.info("PubSubidentifier"+ arguments[0]+ " Topic"+ arguments[1]);	
		while (!cancelled) {
			try {

				messages = Subscriber.pullMessages(pubsub, arguments[0], arguments[1]);
				
				if (messages != null) {
			        for (String msg : messages) {
			            logger.info("message: " + msg);
			        }
			    }
				sleep(20000);

			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}// while
		}
		else{
			System.out.println("Not enough arguments");
		}
	}//Thread run

	public void cancel() {
		cancelled = true;
	}

	public boolean isCancelled() {
		return cancelled;
	}
}
