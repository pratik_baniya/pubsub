import java.io.IOException;

import com.google.api.services.pubsub.Pubsub;

public class MainSubscriber extends PortableConfiguration{
	

	public static void main(String[] args) throws IOException{
		//creating Pubsub Client
		Pubsub pubsub =   createPubsubClient();
		
		
		//Pull request where the message is received every 20 seconds
		Thread t1 = new Thread(new SubThread(pubsub, args));
		t1.start();
	}
}
