
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.api.services.pubsub.Pubsub;
import com.google.api.services.pubsub.model.ListSubscriptionsResponse;
import com.google.api.services.pubsub.model.ListTopicsResponse;
import com.google.api.services.pubsub.model.Subscription;
import com.google.api.services.pubsub.model.Topic;
import com.google.api.services.pubsub.model.AcknowledgeRequest;
import com.google.api.services.pubsub.model.PubsubMessage;
import com.google.api.services.pubsub.model.PullRequest;
import com.google.api.services.pubsub.model.PullResponse;
import com.google.api.services.pubsub.model.ReceivedMessage;
public class Subscriber {
	
	 /*
     * List topics in your project
     * */
    public static void listTopics( Pubsub pubsub) throws IOException{
    	
    	Pubsub.Projects.Topics.List listMethod =
    	        pubsub.projects().topics().list("projects/massive-seer-107519");
    	String nextPageToken = null;
    	ListTopicsResponse response;
    	do {
    	    if (nextPageToken != null) {
    	        listMethod.setPageToken(nextPageToken);
    	    }
    	    response = listMethod.execute();
    	    List<Topic> topics = response.getTopics();
    	    if (topics != null) {
    	        for (Topic topic : topics) {
    	            System.out.println("Found topic: " + topic.getName());
    	        }
    	    }
    	    nextPageToken = response.getNextPageToken();
    	} while (nextPageToken != null);
    }//listTopics
    
    /*
     * Subscribe to a Topic 
     * */
    public static boolean subscribeTopic( Pubsub pubsub,String PubSubidentifier, String Topic, String newsub) throws IOException{
    	 Subscription subscription = new Subscription()
         // The name of the topic from which this subscription
         // receives messages
         .setTopic("projects/"+PubSubidentifier+"/topics/"+Topic)
         // Acknowledgement deadline in second
         .setAckDeadlineSeconds(30);
    	 
		 Subscription newSubscription = pubsub.projects().subscriptions().create(
		         "projects/"+PubSubidentifier+"/subscriptions/"+newsub, subscription)
		         .execute();
		 if (newSubscription == null)
		 {
			 return false;
		 }
		 else 
		 {
			 System.out.println("Created: " + newSubscription.getName());
			 return true;
		}//if creating new subscription is a success
		 
    	
    }//subscribeTopic
    
    /*
     * Receiving Pull Messages
     * */
    
    
    public static List<String> pullMessages(Pubsub pubsub, String PubSubidentifier, String Topic)throws IOException{
    	String subscriptionName =
    	        "projects/"+PubSubidentifier+"/subscriptions/"+Topic;
    	
    	// You can fetch multiple messages with a single API call.
    	int batchSize = 1000;
    	PullRequest pullRequest = new PullRequest()
    	        // Setting ReturnImmediately to false instructs the API to
    	        // wait to collect the message up to the size of
    	        // MaxEvents, or until the timeout.
    	        .setReturnImmediately(false)
    	        .setMaxMessages(batchSize);
    	do {
    	    PullResponse pullResponse = pubsub.projects().subscriptions()
    	            .pull(subscriptionName, pullRequest).execute();
    	    List<String> ackIds = new ArrayList<String>(batchSize);
    	    List<ReceivedMessage> receivedMessages = new ArrayList<ReceivedMessage>(); 	
    	   receivedMessages =
    	    	            pullResponse.getReceivedMessages();
    	    List<String> message = new ArrayList<String>();
    	    if (receivedMessages == null || receivedMessages.isEmpty()) {
    	        // The result was empty.
    	    	message = null;
    	        System.out.println("There were no messages.");
    	        continue;
    	    }
    	    
    	    System.out.println("Pull Message/ received Message size:" + receivedMessages.size());
    	    for (ReceivedMessage receivedMessage : receivedMessages) {
    	        PubsubMessage pubsubMessage = receivedMessage.getMessage();
    	        if (pubsubMessage != null) {
    	            System.out.print("xxxxxxxxxx Message: ");
    	            System.out.println(
    	                    new String(pubsubMessage.decodeData(), "UTF-8"));
    	          
    	            message.add(new String(pubsubMessage.decodeData(), "UTF-8"));
    	        }
    	        ackIds.add(receivedMessage.getAckId());	    
    	    
    	    }
    	    /*
    	    //message is added to Jedis
    	    if (message != null)
    	    	Jedis.addHash(pubsub, message);
    	   */
    	    // Ack can be done asynchronously if you care about throughput.
    	    AcknowledgeRequest ackRequest =
    	            new AcknowledgeRequest().setAckIds(ackIds);
    	    pubsub.projects().subscriptions()
    	            .acknowledge(subscriptionName, ackRequest).execute();
    	    // You can keep pulling messages by changing the condition below.
    	    
    	    return message;
    	} while (false);
		return null;
    	
    }//PullMessages
  
    /*
     * Get the list of Subscription
     * */
    public static void SubscriptionList(Pubsub pubsub) throws IOException
    {
    	Pubsub.Projects.Subscriptions.List listMethod = pubsub.projects()
    	        .subscriptions().list("projects/massive-seer-107519");
    	String nextPageToken = null;
    	ListSubscriptionsResponse response;
    	do {
    	    if (nextPageToken != null) {
    	        listMethod.setPageToken(nextPageToken);
    	    }
    	    response = listMethod.execute();
    	    List<Subscription> subscriptions = response.getSubscriptions();
    	    if (subscriptions != null) {
    	        for (Subscription subscription : subscriptions) {
    	            System.out.println(
    	                    "Found subscription: " + subscription.getName());
    	        }
    	    }
    	    nextPageToken = response.getNextPageToken();
    	} while (nextPageToken != null);
    }//SubscriptionList
    	
    
}//Subscriber class

