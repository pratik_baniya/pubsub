import static org.junit.Assert.*;

import java.io.IOException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.google.api.services.pubsub.Pubsub;

public class SubscriberTest extends PortableConfiguration {
	//Variables
	String[] args ={"massive-seer-107519", "newTopic"};
	String PubSubidentifier = args[0];
	String Topic = args[1];
	List<String> messages;
	Pubsub pubsub;
	
	@Before
	public void main() throws IOException{
		 pubsub = createPubsubClient();
	}
	
	@Test
	public void SubscribeMessages() throws IOException {	
		
		messages = Subscriber.pullMessages(pubsub, PubSubidentifier, "newSubs");
			assertTrue(messages.size() > 0);
	}//Subscribe for new messages
	
	@Test
	public void Make_New_Subscription() throws IOException{
		boolean result =
						Subscriber.subscribeTopic( pubsub, PubSubidentifier, Topic,  "newsub");
		
		assertTrue(result);
	}//Make new subscription for the Topic

}//SubscriberTest Class
