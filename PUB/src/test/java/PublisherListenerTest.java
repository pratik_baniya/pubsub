import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.google.api.services.pubsub.Pubsub;

import static org.mockito.Mockito.*;


public class PublisherListenerTest extends PortableConfiguration{
	String[] args ={"massive-seer-107519", "newTopic", "c", "c"};
	String PubSubidentifier = args[0];
	String Topic = args[1];
	
	
	@Mock
	Publisher pub;
	
	@Before
	public void setup(){
		MockitoAnnotations.initMocks(this);		
	}
	
	@Test
	public void size_of_arguements() {
		
		boolean result = PubListener.ArgumentSize(args);
		assertTrue(result);	
	}//check the if the size is greater than 2
	
	@Test
	public void check_publish_messages() throws IOException{
		
		Pubsub pubsub = createPubsubClient();
		boolean result = PubListener.PublishMessages(args, pubsub, PubSubidentifier, Topic);
		assertTrue(result);
	}
	
	@Test(expected=IOException.class)
	public void TestMainMethod() throws IOException {
	    PubListener.main(args);
	}

}//PublisherListenerTest Class
