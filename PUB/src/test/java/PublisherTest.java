import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import com.google.api.services.pubsub.Pubsub;

public class PublisherTest extends PortableConfiguration{
	String[] args ={"massive-seer-107519", "newTopic", "c", "c"};
	String PubSubidentifier = args[0];
	String Topic = args[1];
	
	@Test
	public void check_publish_messages() throws IOException{
		
		Pubsub pubsub = createPubsubClient();
		boolean result = PubListener.PublishMessages(args, pubsub, PubSubidentifier, Topic);
		assertTrue(result);
	}
}
