import java.io.IOException;

//import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;
//import org.apache.log4j.Logger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.services.pubsub.Pubsub;

public class PubListener extends PortableConfiguration{
	//private static final Logger log = Logger.getLogger(PubListener.class);
	private static final Logger logger = LoggerFactory.getLogger(PubListener.class);
	
	public static void main(String[] args)throws IOException {
		
		//local variables
		String PubSubidentifier;
		String Topic;
		
		//Arguments
		String[] arguments = args;
		
		
		//creating Pubsub Client
		Pubsub pubsub = createPubsubClient();
		PropertyConfigurator.configure("src/main/resources/log4j.properties");
		/*
		 * First parameter is the Pubsub identifier
		 * Second parameter is the Topic 
		 * All other arguments are messages
		 * Check if the length of the arguments passed if greater then 2
		 */
	
		if (ArgumentSize(arguments) == true)
		{
			PubSubidentifier = arguments[0];
			Topic = arguments[1];
			//Logging
			logger.info("Message Received as a parameter:");
			logger.info("PubSubidentifier"+ arguments[0]+ " Topic"+ arguments[1]);
			
			System.out.println(PubSubidentifier+ " " +Topic);
			if (PublishMessages(arguments, pubsub, PubSubidentifier, Topic) == true)
			{
				System.out.println("Message are successfully Published!");
				logger.info("Message are successfully Published!");
			}
			else {
				System.out.println("Message were Not Published!");
				logger.info("Message were Not Published!");
			}
			
		}//if statement
		

		return;
	}//Main
	
	/*
	 * Calling Publish message to Topic from Publisher class
	 * return true if there are any messages to publish
	 * return false if there are no messages to publish
	 * */
	public static boolean PublishMessages(String[] arguments, Pubsub pubsub, String PubSubidentifier, String Topic) throws IOException
	{
		boolean result = false;
		
		for (int i = 2; i < arguments.length; i++) {
			
			result = Publisher.messagetoTopic(pubsub, PubSubidentifier, Topic , arguments[i] );
			System.out.println("Message: "+arguments[i]);
			logger.info("Message: "+arguments[i]);
		}
		return result;
	}
	
	/*
	 * Check the size of the arguments that has been passed*/
	public static boolean ArgumentSize(String[] args)
	{
		int length = args.length;
		if (length >2)	
			return true;
		else {
			System.out.println("Insufficient parameters");
			logger.info("Insufficient parameters");
			return false;
		}
		
	}//
}//PubListener
