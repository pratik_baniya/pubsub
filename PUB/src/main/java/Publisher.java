

import java.io.IOException;
import java.util.List;

import com.google.api.services.pubsub.Pubsub;
import com.google.api.services.pubsub.model.PublishRequest;
import com.google.api.services.pubsub.model.PublishResponse;
import com.google.api.services.pubsub.model.PubsubMessage;
import com.google.common.collect.ImmutableList;

public class Publisher {
	   //Variables for random string
	   static int leftLimit = 97; // letter 'a'
	   static int rightLimit = 122; // letter 'z'
	   static  int targetStringLength = 8;
	   
	   static String  generatedString;

	 
	  /*
	  * Create a message to Topic
	  * */
	  public static boolean messagetoTopic(Pubsub pubsub, String identifier, String topic, String tempMessage) throws IOException{

	    String message = tempMessage;
	    PubsubMessage pubsubMessage = new PubsubMessage();
	    // You need to base64-encode your message with
	    // PubsubMessage#encodeData() method.
	    pubsubMessage.encodeData(message.getBytes("UTF-8"));
	    List<PubsubMessage> messages = ImmutableList.of(pubsubMessage);
	    PublishRequest publishRequest =
	            new PublishRequest().setMessages(messages);
	    PublishResponse publishResponse = pubsub.projects().topics()
	            .publish("projects/"+ identifier+"/topics/"+ topic, publishRequest)
	            .execute();
	   
	    
	    List<String> messageIds = publishResponse.getMessageIds();
	    if (messageIds != null) {
	        for (String messageId : messageIds) {
	            System.out.println("messageId: " + messageId);
	        }
	        return true;
	    }
	    return false;
	    }//messagetoTopic	
	  
	
}//Publisher Class
